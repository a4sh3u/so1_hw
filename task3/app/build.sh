#!/usr/bin/env bash
mvn -B package
docker build -t a4sh3u/spring-boot-app:1.0 .
docker push a4sh3u/spring-boot-app:1.0
