# Task 1 - Elasticsearch alert

Simply run the `task1.sh` to find out if the string  *"Handbill not printed"* appears more than 3 times in Elasticsearch

Assumption - elasticsearch is running locally like this

```
docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:6.7.0
```
