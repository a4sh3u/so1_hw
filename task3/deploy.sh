#!/usr/bin/env bash

kubectl apply -f ns.yaml
kubectl apply -f db.yaml
kubectl apply -f app.yaml
