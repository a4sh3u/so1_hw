# Task 3 - k8s spring-boot app with mysql db

1. **CI for the app** : A docker image is needed for it to be deployed in a kubernetes cluster. So, the `app` directory has been cloned from [spring-boot-app]('https://github.com/TechPrimers/docker-mysql-spring-boot-example') and the `build.sh` script builds the image and pushes it to dockerhub.

2. **Deployment to kubernetes** : Assuming that a kubernetes cluster is already running, simply run the `deploy.sh` script to create the database and spring-boot-app services in a new namespace.

3. **HA of database** : For persistence, PersistentVolumee can be created either on host or cloud and then using PersistentVolumeClaim to utilize it as the storage backend for the mysql database. Afterwards, define volumes and volumeMounts in `db.yaml` to use the storage.

4. **Splitting Deployment for different environments** : Define a new namespace for the new environments and then use those new namespaces in `db.yaml` and `app.yaml` for deployment.

5. **Monitoring the cluster and both services** : Use helm to deploy prometheus along with node exporter (DaemonSet - one pod every node) and blackbox exporter (for database and spring-boot-app services). For this, a prometheus config values file needs to be created for running the **_prometheus_** server, **_alertmanager_** server (for sending alerts), **_nodeexporter_** (for sending every node data to prometheus) and **_blackboxexporter_** (to monitor the two services). In addition, alert files are also needs to be created so that alerts can be generated when node resources are above certain thresholds or if any of the services goes down / slow.
