#!/usr/bin/env bash

max_occurence=3
es_search=$(curl -s -X GET "localhost:9200/_search" -H 'Content-Type: application/json' -d'
{
    "query": {
        "query_string" : {
            "default_field" : "content",
            "query" : "Handbill not printed"
        }
    }
}
' | jq -r .hits.total)

# echo "$es_search"

if [ "$es_search" -gt "$max_occurence" ]; then
  echo "P1 - Alert - Handbill not printed for more than 3 times"
fi
